"""
https://leetcode.com/problems/leaf-similar-trees/description/

    Consider all the leaves of a binary tree, from left to right order, the
values of those leaves form a leaf value sequence.
"""


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def leafSimilar(self, root1: TreeNode | None, root2: TreeNode | None) -> bool:

        def traversal(node: TreeNode):
            if node is None:
                return []
            if node.left is None and node.right is None:
                return [node.val]
            return traversal(node.left) + traversal(node.right)

        return traversal(root1) == traversal(root2)
