"""
You are given the root of a binary tree where each node has a value 0 or 1. Each root-to-leaf path represents a binary number starting with the most significant bit.

For example, if the path is 0 -> 1 -> 1 -> 0 -> 1, then this could represent 01101 in binary, which is 13.
For all leaves in the tree, consider the numbers represented by the path from the root to that leaf. Return the sum of these numbers.

The test cases are generated so that the answer fits in a 32-bits integer.

https://leetcode.com/problems/sum-of-root-to-leaf-binary-numbers/
"""

from typing import Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


test_head = TreeNode(val=1)
test_head.left = TreeNode(val=0)
test_head.right = TreeNode(val=1)
test_head.left.left = TreeNode(val=0)
test_head.left.right = TreeNode(val=1)
test_head.right.left = TreeNode(val=0)
test_head.right.right = TreeNode(val=1)

class Solution:
    def sumRootToLeaf(self, root: Optional[TreeNode]) -> int:
        self.ans = 0
        def traversal(root, tmp):
            if not root:
                self.ans += int('0b'+''.join(map(str, tmp)), 2)
                tmp = []
                return
            
            tmp.append(root.val)
            if root.left and root.right:
                traversal(root.left, tmp.copy())
                traversal(root.right, tmp.copy())
            elif root.left:
                traversal(root.left, tmp.copy())
            elif root.right:
                traversal(root.right, tmp.copy())
            else:
                traversal(None, tmp.copy())

        traversal(root, [])
        return self.ans

# TODO Разберись как работает.
# class Solution:
#     def sumRootToLeaf(self, root: TreeNode) -> int:
#         root_to_leaf = 0
#         stack = [(root, 0) ]
        
#         while stack:
#             root, curr_number = stack.pop()
#             if root is not None:
#                 curr_number = (curr_number << 1) | root.val
#                 # if it's a leaf, update root-to-leaf sum
#                 if root.left is None and root.right is None:
#                     root_to_leaf += curr_number
#                 else:
#                     stack.append((root.right, curr_number))
#                     stack.append((root.left, curr_number))
                        
#         return root_to_leaf
