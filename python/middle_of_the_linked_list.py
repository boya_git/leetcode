"""
Given the head of a singly linked list, return the middle node of the linked list.

If there are two middle nodes, return the second middle node.

https://leetcode.com/problems/middle-of-the-linked-list/
"""

from typing import Optional


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def middleNode(self, head: Optional[ListNode]) -> Optional[ListNode]:
        arr = [head]
        print(f'Array: {arr}')
        current = head
        for _ in range(self.middle_lenght(head)):
            current = current.next
            
        return current
    def middle_lenght(self, curr, total=0):
        total += 1
        if curr.next == None:
            return total//2
        return self.middle_lenght(curr.next, total)


"""
Omg. From comments.
"""
# class Solution:
#     def middleNode(self, head):
#         slow = fast = head
#         while fast and fast.next:
#             slow = slow.next
#             fast = fast.next.next
#         return slow