"""
Given a binary tree, find its minimum depth.

The minimum depth is the number of nodes along the shortest path from the root
node down to the nearest leaf node.

Note: A leaf is a node with no children.

https://leetcode.com/problems/minimum-depth-of-binary-tree/
"""

from typing import Optional
from collections import deque

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    # Runtime 483 ms (Beats 91.88%)
    # Memory 51 MB (Beats 91.25%)
    def minDepth(self, root: Optional[TreeNode]) -> int:
        if root is None: return 0

        queue = deque()
        queue.append((root, 1))
        while(queue):
            node, lvl = queue.popleft()
            if node.left == node.right == None:
                return lvl
            if node.left:
                queue.append((node.left, lvl+1))
            if node.right:
                queue.append((node.right, lvl+1))
