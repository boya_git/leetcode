"""
https://leetcode.com/problems/kth-smallest-element-in-a-bst/

Given the root of a binary search tree, and an integer k, return the kth
smallest value (1-indexed) of all the values of the nodes in the tree.
"""

from typing import Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:

    def __init__(self):
        self.pointer = 0

    def kthSmallest(self, root: Optional[TreeNode], k: int) -> int:

        def traversal(root: TreeNode):
            if root.left:
                traversal(root.left)

            self.pointer += 1

            if self.pointer == k:
                self.k_node = root
                return

            if root.right:
                traversal(root.right)

        traversal(root)
        return self.k_node.val
