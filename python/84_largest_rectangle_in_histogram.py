"""
https://leetcode.com/problems/largest-rectangle-in-histogram/

Given an array of integers heights representing the histogram's bar height
where the width of each bar is 1, return the area of the largest rectangle
in the histogram.


На каждом шаге добавляем индекс текущего элемента в стек.
Если текущий элемент больше предыдущего (его индекс последний в стеке), то
ничего не происходит, стек просто пополняется. Если же он меньше, то мы
останавливаемся на текущем элементе и начинаем двигаться в обратном направлении
до элемента который был бы не меньше того, на котором мы остановились. Двигаясь
в обратном направлении мы на каждом шаге измеряем длинну "здания" или
пройденного пути от момента остановки и умножаем её на высоту элемента текущего
шага, таким образом получая актуальную площать поверхности. Находя максимум
такой площади мы получим окончательный ответ.
"""

from typing import List


class Solution:
    def largestRectangleArea(self, height: List[int]):
        height.append(0)
        stack = [-1]
        ans = 0
        for i in range(len(height)):
            while height[i] < height[stack[-1]]:
                h = height[stack.pop()]
                w = i - stack[-1] - 1
                ans = max(ans, h * w)
            stack.append(i)
        height.pop()
        return ans
