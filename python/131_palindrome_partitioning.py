"""
Given a string s, partition s such that every substring of the partition is a palindrome. Return all possible palindrome partitioning of s.

A palindrome string is a string that reads the same backward as forward.

https://leetcode.com/problems/palindrome-partitioning/
"""

from typing import List


class Solution:
    def partition(self, s: str) -> List[List[str]]:
        if len(s) == 0: return [[]]
        
        def rec(palindromes, tmp, s):
            
            if len(s) == 0:
                palindromes.append(list(tmp))
                print(f'Current: {palindromes}')
                return

            for i in range(1, len(s)+1):
                if self.is_palindrome(s[:i]):
                    rec(palindromes, tmp+[s[:i]], s[i:])
                    
        palindromes = []
        rec(palindromes, [], s)

        return palindromes
    def is_palindrome(self, s: str):
        return s == s[::-1]
