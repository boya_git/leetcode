"""
Given two binary strings a and b, return their sum as a binary string.

https://leetcode.com/problems/add-binary/
"""

class Solution:
    def addBinary(self, a: str, b: str) -> str:
        if len(a)<len(b):
            a,b = b,a
        b = '0'*(len(a)-len(b)) + b
        ans, tmp = [], 0
        for i in reversed(range(0, len(a))):
            if a[i] == '1' and b[i] == '1':
                ans.append(str(tmp))
                tmp = 1
            elif a[i] == '1' or b[i] == '1':
                if tmp:
                    ans.append('0')
                else:
                    ans.append('1')
                    tmp = 0
            else:
                ans.append(str(tmp))
                tmp = 0
        if tmp:
            ans = str(tmp) + ''.join(ans[::-1])
        else:
            ans = ''.join(ans[::-1])

        return ans
