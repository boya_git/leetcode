"""
https://leetcode.com/problems/sequential-digits/

An integer has sequential digits if and only if each digit in the number is
one more than the previous digit.

Return a sorted list of all the integers in the range [low, high] inclusive
that have sequential digits.
"""

from typing import List


# Solution 1 using list and loops
# Runtime: 28 ms, faster than 92.05% of Python3 online submissions for Sequential Digits.
# Memory Usage: 14.3 MB, less than 57.00% of Python3 online submissions for Sequential Digits.
class Solution:
    def sequentialDigits(self, low: int, high: int) -> List[int]:
        answer = []
        for x in range(len(str(low)), len(str(high))+1):
            mask = int('1'*x)
            number = int(''.join(str(n) for n in range(1, x+1)))

            while number % 10 != 0:
                answer.append(number)
                number += mask
        answer = list(filter(lambda x: low <= x <= high, answer))
        return answer


# Solution 2 using generators and list comprehensions
# Runtime: 32 ms, faster than 79.62% of Python3 online submissions for Sequential Digits.
# Memory Usage: 14.4 MB, less than 24.45% of Python3 online submissions for Sequential Digits.
class Solution:
    def sequentialDigits(self, low: int, high: int) -> List[int]:
        def answer(low: int, high: int):
            for x in range(len(str(low)), len(str(high))+1):
                mask = int('1'*x)
                number = int(''.join(str(n) for n in range(1, x+1)))

                while number % 10 != 0:
                    yield number
                    number += mask

        return [num for num in answer(low, high) if low <= num <= high]
