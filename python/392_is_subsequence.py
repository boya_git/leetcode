"""
https://leetcode.com/problems/is-subsequence/

Given two strings s and t, return true if s is a subsequence of t, or false
otherwise.

A subsequence of a string is a new string that is formed from the original
string by deleting some (can be none) of the characters without disturbing
the relative positions of the remaining characters. (i.e., "ace" is a
subsequence of "abcde" while "aec" is not).
"""
from collections import deque


class Solution:
    def isSubsequence(self, s: str, t: str) -> bool:
        if not s:
            return True

        queue = deque(s)
        for letter in t:
            if letter == queue[0]:
                queue.popleft()
            if not queue:
                return True

        return False
