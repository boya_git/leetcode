"""
Given an array nums of integers, return the length of the longest arithmetic
subsequence in nums.

Note that:

- A subsequence is an array that can be derived from another array by deleting
  some or no elements without changing the order of the remaining elements.
- A sequence seq is arithmetic if seq[i + 1] - seq[i] are all the same value
  (for 0 <= i < seq.length - 1).

https://leetcode.com/problems/longest-arithmetic-subsequence/description/
"""


class Solution:
    # Not my solution.
    def longestArithSeqLength(self, nums: list[int]) -> int:
        if (lenght := len(nums)) <= 2:
            return lenght

        max_arth_len = 2
        dp = [{} for _ in range(lenght)]

        for i in range(lenght):
            for j in range(i):
                diff = nums[i] - nums[j]
                dp[i][diff] = dp[j].get(diff, 1) + 1
                max_arth_len = max(max_arth_len, dp[i][diff])

        return max_arth_len

# Для каждого элемента у нас есть словарь, который хранит кол-во элементов
# последовательности для каждой из разностей для этого элемента.
