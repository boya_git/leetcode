"""
You are given a list of songs where the ith song has a duration of time[i] seconds.

Return the number of pairs of songs for which their total duration in seconds is divisible by 60. Formally, we want the number of indices i, j such that i < j with (time[i] + time[j]) % 60 == 0.

https://leetcode.com/problems/pairs-of-songs-with-total-durations-divisible-by-60/
"""


from typing import List

class Solution:
    def numPairsDivisibleBy60(self, time: List[int]) -> int:
        cache = dict()
        result = 0
        for t in time:
            if t%60 in cache:
                result += cache[t%60]
            if (60-t%60)%60 in cache:
                cache[(60-t%60)%60] += 1
            if (60-t%60)%60 not in cache:
                cache[(60-t%60)%60] = 1
        return result
