"""
https://leetcode.com/problems/container-with-most-water/

    You are given an integer array height of length n. There are n vertical
lines drawn such that the two endpoints of the ith line are (i, 0) and
(i, height[i]).

    Find two lines that together with the x-axis form a container, such that
the container contains the most water.

    Return the maximum amount of water a container can store.

    Notice that you may not slant the container.
"""


class Solution:
    def maxArea(self, height: list[int]) -> int:
        left, right = 0, len(height)-1
        max_area = 0
        while left != right:
            area = (right-left) * min(height[left], height[right])
            if area > max_area:
                max_area = area
            if height[left] >= height[right]:
                right -= 1
            else:
                left += 1
        return max_area
