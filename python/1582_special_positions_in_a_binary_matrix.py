"""
https://leetcode.com/problems/special-positions-in-a-binary-matrix/description/

    Given an m x n binary matrix mat, return the number of special positions
in mat.

    A position (i, j) is called special if mat[i][j] == 1 and all other
elements in row i and column j are 0 (rows and columns are 0-indexed).
"""

class Solution:
    def numSpecial(self, mat: list[list[int]]) -> int:
        ans = 0
        for i in range(len(mat)):
            for j in range(len(mat[0])):
                if mat[i][j]:
                    if sum(self.get_row(mat, i)) == sum(self.get_col(mat, j)) == 1:
                        ans += 1
                    break
        return ans

    def get_row(self, mat, i):
        yield from mat[i]

    def get_col(self, mat, j):
        for i in range(len(mat)):
            yield mat[i][j]
