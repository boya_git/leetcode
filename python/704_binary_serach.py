"""
Given an array of integers nums which is sorted in ascending order,
and an integer target, write a function to search target in nums.
If target exists, then return its index. Otherwise, return -1.

You must write an algorithm with O(log n) runtime complexity.

"""


from typing import List


class Solution:

    def search(self, nums: List[int], target: int) -> int:
        left, right = 0, len(nums) - 1

        while left <= right:
            ref = (left + right) // 2
            if nums[ref] == target:
                return ref
            elif target < nums[ref]:
                right = ref - 1
            elif target > nums[ref]:
                left = ref + 1

        return -1
