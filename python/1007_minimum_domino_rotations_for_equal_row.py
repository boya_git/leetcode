"""
https://leetcode.com/problems/minimum-domino-rotations-for-equal-row/

In a row of dominoes, tops[i] and bottoms[i] represent the top and bottom
halves of the ith domino. (A domino is a tile with two numbers
from 1 to 6 - one on each half of the tile.)

We may rotate the ith domino, so that tops[i] and bottoms[i] swap values.

Return the minimum number of rotations so that all the values in tops are the
same, or all the values in bottoms are the same.

If it cannot be done, return -1.
"""
from typing import List


class Solution:
    def minDominoRotations(self, tops: List[int], bottoms: List[int]) -> int:
        dominos_count = len(tops)
        dominos = {}

        for domino in zip(tops, bottoms):
            if domino[0] == domino[1]:
                dominos[domino[0]] = dominos.get(domino[0], 0) + 1
            else:
                dominos[domino[0]] = dominos.get(domino[0], 0) + 1
                dominos[domino[1]] = dominos.get(domino[1], 0) + 1

        if max(dominos.values()) < dominos_count:
            return -1

        ans = dominos_count
        possibles = [k for k, v in dominos.items() if v == dominos_count]
        for possible in possibles:
            tmp = sum([1 for top in tops if top == possible])
            if ans > dominos_count - tmp:
                ans = dominos_count - tmp

            tmp = sum([1 for bottom in bottoms if bottom == possible])
            if ans > dominos_count - tmp:
                ans = dominos_count - tmp

        return ans
