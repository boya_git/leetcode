"""
You are given a list of bombs. The range of a bomb is defined as the area
where its effect can be felt. This area is in the shape of a circle with
the center as the location of the bomb.

The bombs are represented by a 0-indexed 2D integer array bombs where
bombs[i]= [xi, yi, ri]. xi and yi denote the X-coordinate and Y-coordinate of
the location of the ith bomb, whereas ri denotes the radius of its range.

You may choose to detonate a single bomb. When a bomb is detonated, it will
detonate all bombs that lie in its range. These bombs will further detonate the
bombs that lie in their ranges.

Given the list of bombs, return the maximum number of bombs that can be
detonated if you are allowed to detonate only one bomb.

https://leetcode.com/problems/detonate-the-maximum-bombs/
"""


class Solution:
    def maximumDetonation(self, bombs: list[list[int]]) -> int:
        results = []
        for i in range(len(bombs)):
            bombs_c = bombs.copy()
            queue = [bombs_c[i],]
            bombs_c.remove(bombs_c[i])
            detonated = 1
            while queue:
                bomb = queue.pop()
                bombs_detonated = self.bombs_in_radius(bomb, bombs_c)
                detonated += len(bombs_detonated)
                for b in bombs_detonated:
                    bombs_c.remove(b)
                queue.extend(bombs_detonated)
            results.append(detonated)
        return max(results)

    @staticmethod
    def bombs_in_radius(bomb, bombs):
        bomb_x, bomb_y, radius = bomb
        return [[b_x, b_y, b_r] for b_x, b_y, b_r in bombs if pow(b_x - bomb_x, 2) + pow(b_y - bomb_y, 2) <= pow(radius, 2)]


# Not my best...
