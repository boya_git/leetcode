"""You are given an array of characters letters that is sorted in
non-decreasing order, and a character target. There are at least two different
characters in letters.

Return the smallest character in letters that is lexicographically greater than
target. If such a character does not exist, return the first character in
letters.

https://leetcode.com/problems/find-smallest-letter-greater-than-target/description/
"""

from itertools import dropwhile


class Solution:
    # Runtime 144 ms (Beats 14.93%) | Memory 16.6 MB (Beats 47.30%)
    def nextGreatestLetter(self, letters: list[str], target: str) -> str:
        greater_then_target = filter(lambda letter: letter > target, letters)
        try:
            return min(greater_then_target)
        except ValueError:
            return letters[0]


class Solution2:
    # Runtime 130 ms (Beats 65.85%) | Memory 16.7 MB (Beats 22.38%)
    def nextGreatestLetter(self, letters: list[str], target: str) -> str:
        for letter in letters:
            if letter > target:
                return letter
        return letters[0]


class Solution:
    def nextGreatestLetter(self, letters: list[str], target: str) -> str:
        try:
            return min(dropwhile(lambda x: x <= target, letters))
        except:
            return letters[0]
