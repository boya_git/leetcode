"""
Given a string s consisting of words and spaces, return the length of the
last word in the string.

A word is a maximal substring consisting of non-space characters only.

https://leetcode.com/problems/length-of-last-word/description/
"""


class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        return len(s.strip().split()[-1])


class Solution2:
    def lengthOfLastWord(self, s: str) -> int:
        counter = 0
        for c in  reversed(s):
            if c == " ":
                if not counter:
                    continue
                break
            counter += 1
        return counter


class Solution3:
    def lengthOfLastWord(self, s: str) -> int:
        *_, last_word = s.split()
        return len(last_word)
