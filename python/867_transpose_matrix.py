"""
https://leetcode.com/problems/transpose-matrix/description/

    Given a 2D integer array matrix, return the transpose of matrix.
    The transpose of a matrix is the matrix flipped over its main diagonal,
switching the matrix's row and column indices.
"""


class Solution:
    # 66ms Beats 91.33%
    # 17.16MB Beats 53.63%
    def transpose(self, matrix: list[list[int]]) -> list[list[int]]:
        return [[matrix[i][j] for i in range(len(matrix))]
                for j in range(len(matrix[0]))]
        # 72 ms Beats 68.14%
        # 17.1 MB Beats 53.63%
        # return list(zip(*matrix))
