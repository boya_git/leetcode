"""
https://leetcode.com/problems/subarray-sum-equals-k/

Given an array of integers nums and an integer k, return the total number of
continuous subarrays whose sum equals to k.
"""
from typing import List


class Solution:
    def subarraySum(self, nums: List[int], k: int) -> int:
        cases = {0: 1}
        tmp = 0
        count = 0
        for item in nums:
            tmp += item
            if (tmp-k) in cases.keys():
                count += cases.get((tmp-k))
            cases[tmp] = cases.get(tmp, 0) + 1
        return count
