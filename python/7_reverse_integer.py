"""
https://leetcode.com/problems/reverse-integer/

Given a signed 32-bit integer x, return x with its digits reversed.
If reversing x causes the value to go outside the signed 32-bit integer range
[-231, 231 - 1], then return 0.
"""


class Solution:
    def reverse(self, x: int) -> int:
        sx = str(x)
        sign = '-' if sx[0] == '-' else '+'
        sx = sx.replace('-', '')
        ans = int(sign + sx[::-1])
        return ans if pow(-2, 31) < ans < pow(2, 31)-1 else 0
