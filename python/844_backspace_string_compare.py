"""
Given two strings s and t, return true if they are equal when both are typed
into empty text editors. '#' means a backspace character.

Note that after backspacing an empty text, the text will continue empty.

https://leetcode.com/problems/backspace-string-compare/
"""


class Solution:
    def backspaceCompare(self, s: str, t: str) -> bool:
        def remove_backspaces(string: str) -> str:
            stack = []
            for c in string:
                if c == '#':
                    if stack:
                        stack.pop()
                else:
                    stack.append(c)
            return ''.join(stack)
        return remove_backspaces(s) == remove_backspaces(t)
