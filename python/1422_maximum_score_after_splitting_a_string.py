"""
https://leetcode.com/problems/maximum-score-after-splitting-a-string/

    Given a string s of zeros and ones, return the maximum score after splitting
the string into two non-empty substrings (i.e. left substring and right substring).

    The score after splitting a string is the number of zeros in the left
substring plus the number of ones in the right substring.
"""


class Solution:
    def maxScore(self, s: str) -> int:
        left = 0
        right = s.count('1')
        ans = 0
        for si in s[0:len(s)-1]:
            if si == "1":
                right -= 1
            else:
                left += 1
            ans = max(ans, left+right)
        return ans
