"""
https://leetcode.com/problems/baseball-game/

You are keeping score for a baseball game with strange rules. The game consists
of several rounds, where the scores of past rounds may affect future rounds'
scores.

At the beginning of the game, you start with an empty record. You are given a
list of strings ops, where ops[i] is the ith operation you must apply to the
record and is one of the following:
    1. An integer x - Record a new score of x.
    2. "+" - Record a new score that is the sum of the previous two scores. It
       is guaranteed there will always be two previous scores.
    3. "D" - Record a new score that is double the previous score. It is
       guaranteed there will always be a previous score.
    4. "C" - Invalidate the previous score, removing it from the record. It is
       guaranteed there will always be a previous score.
Return the sum of all the scores on the record.
"""

from typing import List


class Solution:

    """
    Runtime: 65 ms, faster than 37.03% of Python3 online submissions.
    Memory Usage: 14 MB, less than 74.58% of Python3 online submissions.
    """

    def calPoints(self, ops: List[str]) -> int:
        result = []
        specials = {'C', 'D', '+'}
        for item in ops:
            if item not in specials:
                result.append(int(item))
            elif item == 'C':
                result.pop()
            elif item == 'D':
                result.append(int(result[-1])*2)
            elif item == '+':
                result.append(int(result[-1])+int(result[-2]))
        return sum(result)


class Solution2:

    """
    Runtime: 67 ms, faster than 33.41% of Python3 online submissions.
    Memory Usage: 14.1 MB, less than 74.58% of Python3 online submissions.
    """

    def calPoints(self, ops: List[str]) -> int:
        stack = []
        for item in ops:
            match item:
                case 'C':
                    stack.pop()
                case 'D':
                    stack.append(int(stack[-1])*2)
                case '+':
                    stack.append(int(stack[-1])+int(stack[-2]))
                case _:
                    stack.append(int(item))
        return sum(stack)
