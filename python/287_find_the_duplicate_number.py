"""
https://leetcode.com/problems/find-the-duplicate-number/

Given an array of integers nums containing n + 1 integers where each integer
is in the range [1, n] inclusive.

There is only one repeated number in nums, return this repeated number.

You must solve the problem without modifying the array nums and uses only
constant extra space.
"""

from typing import List


class Solution:
    def findDuplicate(self, nums: List[int]) -> int:
        uniq = set(nums)
        for num in nums:
            if num in uniq:
                uniq.remove(num)
            else:
                return num
