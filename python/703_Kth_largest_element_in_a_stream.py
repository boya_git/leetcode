"""
https://leetcode.com/problems/kth-largest-element-in-a-stream/

Design a class to find the kth largest element in a stream. Note that it is the
kth largest element in the sorted order, not the kth distinct element.

Implement KthLargest class:
 - KthLargest(int k, int[] nums) Initializes the object with the integer k and
 the stream of integers nums.
 - int add(int val) Appends the integer val to the stream and returns the
 element representing the kth largest element in the stream.
"""

import bisect
import heapq

from typing import List


class KthLargest:

    """
    Runtime: 117 ms, faster than 71.53% of Python3 online submissions.
    Memory Usage: 18.1 MB, less than 87.74% of Python3 online submissions.
    """

    def __init__(self, k: int, nums: List[int]):
        self.k = k
        self.heap = nums

        heapq.heapify(self.heap)

        while len(self.heap) > self.k:
            heapq.heappop(self.heap)

    def add(self, val: int) -> int:
        heapq.heappush(self.heap, val)

        if len(self.heap) > self.k:
            heapq.heappop(self.heap)
        return self.heap[0]


class KthLargest2:

    """
    Runtime: 148 ms
    Memory Usage: 18.3 MB
    """

    def __init__(self, k: int, nums: List[int]):
        self.k = k
        self.nums = sorted(nums)

    def add(self, val: int) -> int:
        bisect.insort(self.nums, val)
        return self.nums[-self.k]
