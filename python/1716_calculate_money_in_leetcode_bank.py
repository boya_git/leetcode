"""
https://leetcode.com/problems/calculate-money-in-leetcode-bank/description/

    Hercy wants to save money for his first car. He puts money in the Leetcode bank
every day.
    He starts by putting in $1 on Monday, the first day. Every day from Tuesday
to Sunday, he will put in $1 more than the day before. On every subsequent
Monday, he will put in $1 more than the previous Monday.
    Given n, return the total amount of money he will have in the Leetcode bank
at the end of the nth day.
"""

class Solution:
    def totalMoney(self, n: int) -> int:
        weeks, days = divmod(n, 7)
        return sum(28+x*7 for x in range(weeks)) + sum(range(weeks+1, weeks+1+days))
        # I can use it instead:
        # return 28*weeks + (0+7*(weeks-1))*weeks//2 + (1+weeks + weeks+days)*days//2
