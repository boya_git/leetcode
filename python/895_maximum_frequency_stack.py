"""
https://leetcode.com/problems/maximum-frequency-stack/

Design a stack-like data structure to push elements to the stack and pop the
most frequent element from the stack.

Implement the FreqStack class:

FreqStack() constructs an empty frequency stack.
void push(int val) pushes an integer val onto the top of the stack.
int pop() removes and returns the most frequent element in the stack.
If there is a tie for the most frequent element, the element closest to the
stack's top is removed and returned.
"""


class FreqStack:

    def __init__(self):
        self.stack = []
        self.freq = {}

    def push(self, val: int) -> None:
        self.stack.append(val)
        self.freq[val] = self.freq.get(val, 0) + 1

    def pop(self) -> int:
        most_freq = max(self.freq.values())
        for i in reversed(range(len(self.stack))):
            val = self.stack[i]
            if self.freq[val] == most_freq:
                self.freq[val] -= 1
                del self.stack[i]
                return val
