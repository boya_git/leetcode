"""
You are given the head of a singly linked-list. The list can be represented as:

L0 → L1 → … → Ln - 1 → Ln
Reorder the list to be on the following form:

L0 → Ln → L1 → Ln - 1 → L2 → Ln - 2 → …
You may not modify the values in the list's nodes. Only nodes themselves may be changed.

https://leetcode.com/problems/reorder-list/
"""

from typing import Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

def to_ll(lst=[0,], next=None):
    try:
        val = lst.pop()
    except IndexError:
        return next
    current = ListNode(val, next)
    return to_ll(lst, current)

def view_ll(current: ListNode):
    print(current.val)
    if not current.next:
        return 0
    return view_ll(current.next)
        
# O(1)
def lenght_ll(current, lenght=0):
    lenght += 1
    if current.next:
        return lenght_ll(current.next, lenght)
    else:
        return lenght

# O(1)
def find_ancor(head, point):
    if point:
        return find_ancor(head.next, point-1)
    else:
        return head 

def reverse_by_ancor(prev, current):
    next = current.next
    current.next = prev
    if not next:
        return current
    return reverse_by_ancor(current, next)
    

class Solution:
    def reorderList(self, head: Optional[ListNode]) -> None:
        length = lenght_ll(head)
        if length in [1,2]:
            return
        self.ancor = find_ancor(head, round(length//2))
        last = reverse_by_ancor(self.ancor, self.ancor.next)
        self.ancor.next = None
        self.reorder(head, last)

    def reorder(self, head, last):
        n_head = head.next
        n_last = last.next
        if last.next == self.ancor:
            head.next = last
            last.next = n_head
            return
        head.next = last
        last.next = n_head
        return self.reorder(n_head, n_last)
