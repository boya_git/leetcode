"""
Given two non-negative integers num1 and num2 represented as strings, return the product of num1 and num2, also represented as a string.

Note: You must not use any built-in BigInteger library or convert the inputs to integer directly.

https://leetcode.com/problems/multiply-strings/
"""

class Solution:
    def multiply(self, num1: str, num2: str) -> str:
        num1_i = self.convert_int(num1)
        num2_i = self.convert_int(num2)
        return str(num1_i*num2_i)

    @staticmethod
    def convert_int(s: str):
        values = {'0':0,'1':1,'2':2,'3':3,'4':4,'5':5,'6':6,'7':7,'8':8,'9':9,}
        if not s:
            return 0
        elif len(s) == 1:
            return values[s]
        else:
            return sum([pow(10, p)*values[d] for p,d in enumerate(s[::-1])])
