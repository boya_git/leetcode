"""
On an infinite plane, a robot initially stands at (0, 0) and faces north. The robot can receive one of three instructions:

"G": go straight 1 unit;
"L": turn 90 degrees to the left;
"R": turn 90 degrees to the right.
The robot performs the instructions given in order, and repeats them forever.

Return true if and only if there exists a circle in the plane such that the robot never leaves the circle.

https://leetcode.com/problems/robot-bounded-in-circle/
"""
movement = {
    'N': (0,1),
    'S': (0,-1),
    'E': (1,0),
    'W': (-1,0),
}
directions = {
    'N': {
        'L': 'W',
        'R': 'E',
    },
    'S': {
        'L': 'E',
        'R': 'W',
    },
    'E': {
        'L': 'N',
        'R': 'S',
    },
    'W': {
        'L': 'S',
        'R': 'N',
    },
}

class Solution:
    def isRobotBounded(self, instructions: str) -> bool:
        if instructions in ['L', 'R', 'RG', 'GR', 'LG', 'GL']:
            return True
        
        robot = {'coords': [0,0], 'direction': 'N',}
        for _ in range(2):
            for action in  instructions:
                if action == 'G':
                    robot['coords'][0] += movement[robot['direction']][0]
                    robot['coords'][1] += movement[robot['direction']][1]
                else:
                    robot['direction'] = directions[robot['direction']][action]
        return robot == {'coords': [0,0], 'direction': 'N',}
