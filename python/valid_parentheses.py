"""
20. Valid Parentheses

Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:
    - Open brackets must be closed by the same type of brackets.
    - Open brackets must be closed in the correct order.

https://leetcode.com/problems/valid-parentheses/
"""

class Solution:
    def isValid(self, s: str) -> bool:
        for _ in range(1, int(len(s)/2+1)):
            s = s.replace('()', '').replace('[]', '').replace('{}', '')
        return False if s else True
    
    def isValid2(self, s: str) -> bool:
        if len(s)%2:
            return False
        stack = []
        parentheses = {')': '(', 
                       '}': '{', 
                       ']': '[', }
        for item in s:
            if item in parentheses.values():
                stack.append(item)
            if item in parentheses.keys():
                try:
                    if stack.pop() != parentheses.get(item):
                        return False 
                except:
                    return False
        return not stack
