"""
You are given two integer arrays nums1 and nums2 sorted in ascending order and
an integer k.

Define a pair (u, v) which consists of one element from the first array and one
element from the second array.

Return the k pairs (u1, v1), (u2, v2), ..., (uk, vk) with the smallest sums.

https://leetcode.com/problems/find-k-pairs-with-smallest-sums/description/
"""


import heapq

class Solution:
    # Not my solution
    def kSmallestPairs(self, nums1: list[int], nums2: list[int], k: int) -> list[list[int]]:
        n = len(nums1)
        m = len(nums2)
        heap = []
        seen = set()
        ans = []
        heapq.heappush(heap, (nums1[0]+nums2[0], 0, 0))
        seen.add((0, 0))
        while heap and k > 0:
            min_val, i, j = heapq.heappop(heap)
            ans.append([nums1[i], nums2[j]])
            if i < n-1 and (i+1, j) not in seen:
                heapq.heappush(heap, (nums1[i+1] + nums2[j], i+1, j))
                seen.add((i+1, j))
            if j < m-1 and (i, j+1) not in seen:
                heapq.heappush(heap, (nums1[i] + nums2[j+1], i, j+1))
                seen.add((i, j+1))
            k -= 1
        return ans
