"""
https://leetcode.com/problems/destination-city/description/


    You are given the array paths, where paths[i] = [cityAi, cityBi] means there
exists a direct path going from cityAi to cityBi. Return the destination city,
that is, the city without any path outgoing to another city.

    It is guaranteed that the graph of paths forms a line without any loop,
therefore, there will be exactly one destination city.
"""


class Solution:

    def destCity(self, paths: list[list[str]]) -> str:
        cities_from = {path[0] for path in paths}
        for from_city, to_city in paths:
            if to_city not in cities_from:
                return to_city
