"""
You are given a 0-indexed array nums of n integers, and an integer k.

The k-radius average for a subarray of nums centered at some index i with the
radius k is the average of all elements in nums between the indices i - k and
i + k (inclusive). If there are less than k elements before or after the index
i, then the k-radius average is -1.

Build and return an array avgs of length n where avgs[i] is the k-radius
average for the subarray centered at index i.

The average of x elements is the sum of the x elements divided by x, using
integer division. The integer division truncates toward zero, which means
losing its fractional part.

- For example, the average of four elements 2, 3, 1, and 5 is
(2 + 3 + 1 + 5) / 4 = 11 / 4 = 2.75, which truncates to 2.

https://leetcode.com/problems/k-radius-subarray-averages/description/
"""


class Solution:

    # Runtime 1578 ms (Beats 92.76%)
    # Memory 32.1 MB (Beats 99.44%)
    @staticmethod
    def get_averages(nums: list[int], k: int) -> list[int]:
        window_size = k*2+1
        if window_size > len(nums):
            yield from (-1 for _ in range(len(nums)))
        else:
            sum_avg = sum(nums[:window_size])
            yield from [-1]*k
            yield int(sum_avg/window_size)
            for i in range(window_size, len(nums)):
                sum_avg = sum_avg + nums[i] - nums[i-window_size]
                yield int(sum_avg/window_size)
            yield from [-1]*k
