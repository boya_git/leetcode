"""
https://leetcode.com/problems/minimum-number-of-operations-to-make-array-empty/description/

    You are given a 0-indexed array nums consisting of positive integers.

    There are two types of operations that you can apply on the array any number
of times:
- Choose two elements with equal values and delete them from the array.
- Choose three elements with equal values and delete them from the array.
    Return the minimum number of operations required to make the array empty, or
-1 if it is not possible.
"""

from collections import Counter


class Solution:
    def minOperations(self, nums: list[int]) -> int:
        ans = 0
        for n, freq in Counter(nums).items():
            if freq == 2:
                return -1
            ans += (freq // 3) if freq % 3 == 0 else (freq // 3) + 1
        return ans
