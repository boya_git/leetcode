"""
https://leetcode.com/problems/longest-substring-without-repeating-characters/

Given a string s, find the length of the longest substring without repeating
characters.
"""


class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        ans = 0
        start = 0
        chars = set()
        for i in range(len(s)):
            if s[i] not in chars:
                chars.add(s[i])
                ans = max(ans, len(chars))
            else:
                while s[start] != s[i]:
                    chars.remove(s[start])
                    start += 1
                start += 1
        return ans

# Runtime: 109 ms, faster than 37.09% of Python3 online submissions.
# Memory Usage: 14 MB, less than 92.88% of Python3 online submissions.
