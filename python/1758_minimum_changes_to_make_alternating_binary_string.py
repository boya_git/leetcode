"""
https://leetcode.com/problems/minimum-changes-to-make-alternating-binary-string/description/

    You are given a string s consisting only of the characters '0' and '1'. In
one operation, you can change any '0' to '1' or vice versa.

    The string is called alternating if no two adjacent characters are equal. For
example, the string "010" is alternating, while the string "0100" is not.

    Return the minimum number of operations needed to make s alternating.
"""

from itertools import cycle


class Solution:
    def minOperations(self, s: str) -> int:
        return min(
            sum(p1 != s1 for p1, s1 in zip(cycle("10"), s)),
            sum(p0 != s0 for p0, s0 in zip(cycle("01"), s))
        )
