"""
https://leetcode.com/problems/valid-anagram/description/

Given two strings s and t, return true if t is an anagram of s, and false otherwise.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the
original letters exactly once.
"""


class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        if len(s) != len(t): return False
        letters = [0]*26
        for i in range(len(s)):
            letters[ord(s[i])-97] += 1
            letters[ord(t[i])-97] -= 1
        for l in letters:
            if l != 0:
                return False
        return True
