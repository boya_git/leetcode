"""
Given an array of integers citations where citations[i] is the number of
citations a researcher received for their ith paper, return the researcher's
h-index.

According to the definition of h-index on Wikipedia: The h-index is defined as
the maximum value of h such that the given researcher has published at least h
papers that have each been cited at least h times.

https://leetcode.com/problems/h-index/description/
"""

class Solution:
    # Runtime 49 ms (Beats 79.52%)
    # Memory 16.6 MB (Beats 86.29%)
    def hIndex(self, citations: list[int]) -> int:
        h_index = len(citations)
        citations.sort()
        for i in range(len(citations)):
            if citations[i] < h_index:
                h_index -= 1
            else:
                break
        return h_index
