"""
https://leetcode.com/problems/convert-an-array-into-a-2d-array-with-conditions/description/

    You are given an integer array nums. You need to create a 2D array from
nums satisfying the following conditions:

    The 2D array should contain only the elements of the array nums.
    Each row in the 2D array contains distinct integers.
    The number of rows in the 2D array should be minimal.
    Return the resulting array. If there are multiple answers, return any of them.

    Note that the 2D array can have a different number of elements on each row.
"""


class Solution:
    def findMatrix(self, nums: list[int]) -> list[list[int]]:
        check_n = set()
        check_i = set()
        ans = [[],]

        while len(check_i) < len(nums):
            for i in range(len(nums)):
                if nums[i] not in check_n and i not in check_i:
                    check_n.add(nums[i])
                    check_i.add(i)
                    ans[-1].append(nums[i])
            check_n.clear()
            ans.append([])
        return ans[:-1]
