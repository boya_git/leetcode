"""
You are given a perfect binary tree where all leaves are on the same level, and every parent has two children. The binary tree has the following definition:

struct Node {
  int val;
  Node *left;
  Node *right;
  Node *next;
}
Populate each next pointer to point to its next right node. If there is no next right node, the next pointer should be set to NULL.

Initially, all next pointers are set to NULL.

https://leetcode.com/problems/populating-next-right-pointers-in-each-node/
"""

from typing import Optional


class Node:
    def __init__(self, val: int = 0, left: 'Node' = None, right: 'Node' = None, next: 'Node' = None):
        self.val = val
        self.left = left
        self.right = right
        self.next = next

class Solution:
    def connect(self, root: 'Optional[Node]') -> 'Optional[Node]':
        if root:
            self.solve(root)
            return root
        else:
            return
    
    def solve(self, current: 'Optional[Node]') -> 'Optional[Node]':
        if current.left == None:
            return
        self.level(current)
        return self.solve(current.left)
    
    def level(self, current: 'Optional[Node]') -> 'Optional[Node]':
        current.left.next = current.right
        if current.next:
            current.right.next = current.next.left
        if current.next == None:
            return
        self.level(current.next)
