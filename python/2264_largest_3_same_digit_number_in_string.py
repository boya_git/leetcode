"""
https://leetcode.com/problems/largest-3-same-digit-number-in-string/description/

You are given a string num representing a large integer. An integer is good if
it meets the following conditions:
- It is a substring of num with length 3.
- It consists of only one unique digit.
Return the maximum good integer as a string or an empty string "" if no such
integer exists.

Note:
- A substring is a contiguous sequence of characters within a string.
- There may be leading zeroes in num or a good integer.
"""


class Solution:
    # 43 ms Beats 56.39%
    # 16.4 MB Beats 5.11%
    def largestGoodInteger(self, num: str) -> str:
        answer = None
        if len(num) == 3:
            return self.is_good(num) or ""
        for i in range(len(num)-2):
            if substr := self.is_good(num[i:i+3]):
                if answer is None:
                    answer = substr
                    continue
                answer = max(answer, substr)
        return answer or ""

    def is_good(self, substr: str):
        return substr if substr.count(substr[0]) == len(substr) else False

class Solution2:
    # 41ms Beats 67.89%
    # 16.49MB Beats 5.11%
    def largestGoodInteger(self, num: str) -> str:
        counter = 1
        current_max = ""
        for i in range(1, len(num)):
            if num[i] == num[i-1]:
                counter += 1
            else:
                counter = 1
            if counter >= 3:
                current_max = max(current_max, num[i])
        return current_max * 3
