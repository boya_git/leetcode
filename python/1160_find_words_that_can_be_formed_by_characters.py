"""
https://leetcode.com/problems/find-words-that-can-be-formed-by-characters/description/

You are given an array of strings words and a string chars.

A string is good if it can be formed by characters from chars (each character
can only be used once).

Return the sum of lengths of all good strings in words.
"""

from typing import List


class Solution:
    def countCharacters(self, words: List[str], chars: str) -> int:
        answer = 0
        for word in words:
            for l in word:
                if word.count(l) > chars.count(l):
                    break
            else:
                answer += len(word)
        return answer
    