"""
Given the roots of two binary trees p and q, write a function to check if they
are the same or not.

Two binary trees are considered the same if they are structurally
identical, and the nodes have the same value.

https://leetcode.com/problems/same-tree/description/
"""

from typing import Optional
from collections import deque

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    # BFS
    def isSameTree(self, p: Optional[TreeNode], q: Optional[TreeNode]) -> bool:
        if not p and not q:
            return True

        queue = deque([(p, q),])

        while queue:
            p, q = queue.popleft()
            if p is None and q:
                return False
            if p and q is None:
                return False
            if p.val != q.val:
                return False
            if p.left or q.left: queue.append((p.left, q.left))
            if p.right or q.right: queue.append((p.right, q.right))
        return True
