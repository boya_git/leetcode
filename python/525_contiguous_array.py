"""
https://leetcode.com/problems/contiguous-array/

Given a binary array nums, return the maximum length of a contiguous subarray
with an equal number of 0 and 1.
"""
from typing import List


class Solution:
    def findMaxLength(self, nums: List[int]) -> int:
        if len(nums) <= 1:
            return 0

        tmp = 0
        ans = {0: -1}
        max_range = 0
        for i in range(len(nums)):
            tmp += 1 if nums[i] else -1
            ans[tmp] = ans.get(tmp, i)
            max_range = max(max_range, i - ans[tmp])
        return max_range
