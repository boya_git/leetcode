"""
Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".

https://leetcode.com/problems/longest-common-prefix/
"""

from typing import List

class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        if not all(strs):
            return ''
        prefix = strs[0]
        for i in range(1, len(strs)):
            if prefix[0] != strs[i][0]:
                return ''
            if prefix != strs[i][:len(prefix)]:
                tmp = []
                for pair in zip(prefix, strs[i]):
                    if pair[0] == pair[1]:
                        tmp.append(pair[0])
                    else:
                        break
                prefix = ''.join(tmp)
        return prefix if prefix else ''