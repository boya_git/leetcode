"""
https://leetcode.com/problems/minimum-time-visiting-all-points/description/

    On a 2D plane, there are n points with integer coordinates points[i] = [xi, yi].
Return the minimum time in seconds to visit all the points in the order given
by points.
    You can move according to these rules:
* In 1 second, you can either:
    - move vertically by one unit,
    - move horizontally by one unit, or
    - move diagonally sqrt(2) units (in other words, move one unit vertically
      then one unit horizontally in 1 second).
* You have to visit the points in the same order as they appear in the array.
* You are allowed to pass through points that appear later in the order, but
  these do not count as visits.
"""


class Solution:
    # 58ms Beats 88.03%
    # 16.24MB Beats 90.50%
    def minTimeToVisitAllPoints(self, points: list[list[int]]) -> int:
        # Using generator, run through all points, calculate times, sum it and return 
        return sum(self.move_to_point(points[i], points[i+1]) for i in range(len(points)-1))

    def move_to_point(self, point_from, point_to):
        # The maximum difference between the coordinates of two points x and y
        # will be the fastest path between the points
        return max(abs(point_from[0] - point_to[0]),
                   abs(point_from[1] - point_to[1]))
    