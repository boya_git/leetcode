"""
https://leetcode.com/problems/counting-bits/

Given an integer n, return an array ans of length n + 1 such that for each i
(0 <= i <= n), ans[i] is the number of 1's in the binary representation of i.
"""

from typing import List


class Solution:
    def countBits(self, n: int) -> List[int]:
        return [bin(x).count('1') for x in range(n+1)]

# Runtime: 144 ms, faster than 42.81% of Python3 online submissions.
# Memory Usage: 20.7 MB, less than 98.20% of Python3 online submissions.
