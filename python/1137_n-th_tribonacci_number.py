"""
https://leetcode.com/problems/n-th-tribonacci-number/description/

    The Tribonacci sequence Tn is defined as follows:
    T0 = 0, T1 = 1, T2 = 1, and Tn+3 = Tn + Tn+1 + Tn+2 for n >= 0.

    Given n, return the value of Tn.
"""


class Solution:

    def tribonacci(self, n: int) -> int:
        if not n:
            return 0

        n1, n2, n3 = 0, 1, 1
        for _ in range(3, n+1):
            n1, n2, n3 = n2, n3, n1+n2+n3
        return n3
