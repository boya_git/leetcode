"""
Given a collection of numbers, nums, that might contain duplicates, return all
possible unique permutations in any order.

https://leetcode.com/problems/permutations-ii/
"""

from itertools import permutations
from typing import List


class Solution:
    def permuteUnique(self, nums: List[int]) -> List[List[int]]:
        perm = permutations(nums)
        uniq_perm = set(perm)
        return list(uniq_perm)
