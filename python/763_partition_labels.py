"""
https://leetcode.com/problems/partition-labels/

You are given a string s. We want to partition the string into as many parts
as possible so that each letter appears in at most one part.

Note that the partition is done so that after concatenating all the parts in
order, the resultant string should be s.

Return a list of integers representing the size of these parts.
"""

from typing import List


class Solution:
    def partitionLabels(self, s: str) -> List[int]:
        intervals = {}
        ans = []

        for i in range(len(s)):
            if s[i] not in intervals.keys():
                intervals[s[i]] = [i, i]
            else:
                intervals[s[i]][1] = i

        start, end = 0, 0
        for interval in intervals.values():
            if interval[0] <= end:
                end = max(interval[1], end)
            else:
                ans.append(end - (start - 1))
                start = interval[0]
                end = interval[1]
        ans.append(end - (start - 1))

        return ans
