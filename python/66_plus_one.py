"""
You are given a large integer represented as an integer array digits, where each digits[i] is the ith digit of the integer. The digits are ordered from most significant to least significant in left-to-right order. The large integer does not contain any leading 0's.

Increment the large integer by one and return the resulting array of digits.

https://leetcode.com/problems/plus-one/
"""

from typing import List

# class Solution:
#     def plusOne(self, digits: List[int]) -> List[int]:
#         rem = 1
#         for i in reversed(range(0,len(digits))):
#             rem, digits[i] = (digits[i]+rem)//10, (digits[i]+rem)%10
#             if not digits[i]:
#                 break
#         if digits and rem:
#             digits = [rem]+digits
#         return digits

class Solution:
    def plusOne(self, digits: List[int]) -> List[int]:
        if digits[-1] < 9:
            digits[-1] += 1
            return digits
        elif len(digits) == 1 and digits[-1] == 9:
            return [1,0]
        else:
            digits[-1] = 0
            digits[0:-1] = self.plusOne(digits[0:-1])
        return digits