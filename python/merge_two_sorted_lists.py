"""
You are given the heads of two sorted linked lists list1 and list2.

Merge the two lists in a one sorted list. The list should be made by splicing together the nodes of the first two lists.

Return the head of the merged linked list.

https://leetcode.com/problems/merge-two-sorted-lists/
"""

from typing import Optional

# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def mergeTwoLists(self, list1: Optional[ListNode], list2: Optional[ListNode]) -> Optional[ListNode]:
        if not list1:
            return list2
        if not list2:
            return list1
    
        head = None
        if list1.val <= list2.val:
            head = list1
            list1 = list1.next
        else:
            head = list2
            list2 = list2.next
        self.solve(list1, list2, head)
        return head
    
    def solve(self, list1, list2, prev=None):
        if not list1:
            prev.next = list2
            return
        if not list2:
            prev.next = list1
            return
        
        if list1.val <= list2.val:
            current = list1
            list1 = list1.next
        else:
            current = list2
            list2 = list2.next
        
        prev.next = current
        return self.solve(list1, list2, current)
