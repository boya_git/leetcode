"""
https://leetcode.com/problems/last-stone-weight/

You are given an array of integers stones where stones[i] is the weight of the
ith stone.

We are playing a game with the stones. On each turn, we choose the heaviest two
stones and smash them together. Suppose the heaviest two stones have weights x
and y with x <= y. The result of this smash is:

If x == y, both stones are destroyed, and
If x != y, the stone of weight x is destroyed, and the stone of weight y has
new weight y - x.
At the end of the game, there is at most one stone left.

Return the smallest possible weight of the left stone. If there are no stones
left, return 0.
"""


from typing import List


class Solution:
    def lastStoneWeight(self, stones: List[int]) -> int:
        while len(stones) > 1:
            if len(stones) == 2:
                if stones[0] == stones[1]:
                    return 0
                return abs(stones[0] - stones[1])
            x1, x2 = sorted([0, 1], key=lambda v: stones[v], reverse=True)
            for i in range(2, len(stones)):
                if stones[i] > stones[x1]:
                    x2 = x1
                    x1 = i
                elif stones[i] > stones[x2]:
                    x2 = i
            if stones[x1] == stones[x2]:
                stones = [stones[i]
                          for i in range(len(stones))
                          if i not in [x1, x2]]
            else:
                stones[x1] = abs(stones[x1] - stones[x2])
                del stones[x2]
        return stones[0]
