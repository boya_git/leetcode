"""
You are given a sorted unique integer array nums.

A range [a,b] is the set of all integers from a to b (inclusive).

Return the smallest sorted list of ranges that cover all the numbers in the
array exactly. That is, each element of nums is covered by exactly one of the
ranges, and there is no integer x such that x is in one of the ranges but not
in nums.

Each range [a,b] in the list should be output as:
(*) "a->b" if a != b
(*) "a" if a == b

https://leetcode.com/problems/summary-ranges/description/
"""

class Solution:
    def summaryRanges(self, nums: list[int]) -> list[str]:
        nums.append(-1)
        start = 0
        answer = []
        for i in range(len(nums)-1):
            if nums[i+1] - nums[i] != 1:
                if start == i:
                    answer.append(f'{nums[i]}')
                else:
                    answer.append(f'{nums[start]}->{nums[i]}')
                start = i+1
        return answer
