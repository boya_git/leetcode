"""
Given a string s which represents an expression, evaluate this expression and return its value. 

The integer division should truncate toward zero.

You may assume that the given expression is always valid. All intermediate results will be in the range of [-231, 231 - 1].

Note: You are not allowed to use any built-in function which evaluates strings as mathematical expressions, such as eval().

 

Example 1:

Input: s = "3+2*2"
Output: 7
Example 2:

Input: s = " 3/2 "
Output: 1
Example 3:

Input: s = " 3+5 / 2 "
Output: 5

https://leetcode.com/problems/basic-calculator-ii/
"""

class Solution:
    def calculate(self, s: str) -> int:
        s = s.replace(' ', '').replace('+', ' ').replace('-', ' -')
        s = [int(item) if self.is_single_number(item) else self.sub_calc(item) 
             for item in s.split()]
        return int(sum(s))
    
    def is_single_number(self, unit: str):
        return not unit.count('*') + unit.count('/')

    def sub_calc(self, expression: str):
        expression = expression.replace('/', ' / ').replace('*', ' * ').split()
        total = int(expression[0])
        for item in zip(expression[1::2],expression[2::2]):
            if item[0] == '/':
                total /= int(item[1])
            elif item[0] == '*':
                total *= int(item[1])
            total = int(total)
        return total
        

s = Solution()
print(s.calculate("3+2*2"))
print(s.calculate(" 3/2 "))
print(s.calculate(" 3+5 / 2 "))
print(s.calculate(" 3+5 / 2*2  -3+5 / 2 "))
print(s.calculate("1337"))
print(s.calculate("12-3*4"))
print(s.calculate("14/3-2"))
print(s.calculate("14/3*2"))
