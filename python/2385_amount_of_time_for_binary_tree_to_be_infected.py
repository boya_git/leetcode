"""
https://leetcode.com/problems/amount-of-time-for-binary-tree-to-be-infected/description/

    You are given the root of a binary tree with unique values, and an integer
start. At minute 0, an infection starts from the node with value start.

    Each minute, a node becomes infected if:
    - The node is currently uninfected.
    - The node is adjacent to an infected node.

    Return the number of minutes needed for the entire tree to be infected.
"""

from collections import defaultdict, deque


# Definition for a binary tree node.
class TreeNode:

    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:

    def amountOfTime(self, root: TreeNode | None, start: int) -> int:
        if root.left is None and root.right is None: return 0
        graph = self.make_graph_from_tree(root)
        ans = self.find_dist_to_edge(graph, start)
        return ans

    def make_graph_from_tree(self, root: TreeNode) -> dict:
        graph = defaultdict(list)
        queue = deque()
        queue.append(root)
        while queue:
            node = queue.popleft()
            if node.left:
                graph[node.val].append(node.left.val)
                graph[node.left.val].append(node.val)
                queue.append(node.left)
            if node.right:
                graph[node.val].append(node.right.val)
                graph[node.right.val].append(node.val)
                queue.append(node.right)
        return graph

    def find_dist_to_edge(self, graph: dict, node_val: int) -> int:
        ans = 0
        seen = set()
        queue = deque()
        queue.append((node_val, 0))
        while queue:
            node, depth = queue.popleft()
            ans = max(ans, depth)
            seen.add(node)
            for children in graph.get(node):
                if children not in seen:
                    queue.append((children, depth + 1))
        return ans
