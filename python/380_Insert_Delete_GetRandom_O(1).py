"""
    380. Insert Delete GetRandom O(1)

    Implement the RandomizedSet class:
    - RandomizedSet() Initializes the RandomizedSet object.
    - bool insert(int val) Inserts an item val into the set if not present. Returns true if the item was not present, false otherwise.
    - bool remove(int val) Removes an item val from the set if present. Returns true if the item was present, false otherwise.
    - int getRandom() Returns a random element from the current set of elements (it's guaranteed that at least one element exists when this method is called). Each element must have the same probability of being returned.

You must implement the functions of the class such that each function works in average O(1) time complexity.
"""

import random


class RandomizedSet:

    def __init__(self):
        self._data : list = []
        self._data_map : dict = {}

    def insert(self, val: int) -> bool: # O(1)
        if val in self._data_map:
            return False

        # create val:index pair in map
        self._data_map[val] = len(self._data) # O(1)
        self._data.append(val) # O(1)
        return True

    def remove(self, val: int) -> bool: # O(1)
        if val not in self._data:
            return False

        # Find last item and index of searched val from map
        last_item = self._data[-1] # O(1)
        val_index = self._data_map.get(val) # O(1)

        # Write last val to instead val and chand last val index in map
        self._data[val_index] = last_item # O(1)
        self._data_map[last_item] = val_index # O(1)

        # remove last item cuz its duplicate, and val:index pair from map
        self._data.pop() # O(1)
        del self._data_map[val] # O(1)
        return True

    def getRandom(self) -> int:
        return random.choice(self._data) # O(?)
