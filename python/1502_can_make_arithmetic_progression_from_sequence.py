"""
A sequence of numbers is called an arithmetic progression if the difference
between any two consecutive elements is the same.

Given an array of numbers arr, return true if the array can be rearranged to
form an arithmetic progression. Otherwise, return false.

https://leetcode.com/problems/can-make-arithmetic-progression-from-sequence/description/
"""


class Solution:
    def canMakeArithmeticProgression(self, arr: list[int]) -> bool:
        s_arr = sorted(arr)
        diff = s_arr[1] - s_arr[0]
        prev = s_arr[1]
        for i in range(1, len(s_arr)):
            if s_arr[i] - s_arr[i-1] != diff:
                return False
        return True

    def solution2(self, arr: list[int]) -> bool:
        st_arr = set(arr)
        min1 = min(st_arr)
        st_arr.remove(min1)
        point = min(st_arr)
        st_arr.remove(point)
        diff = point - min1
        while st_arr:
            if (point + diff) not in st_arr:
                return False
            st_arr.remove(point)
            point += diff
        return True
