"""
Design a HashSet without using any built-in hash table libraries.

Implement MyHashSet class:

- void add(key) Inserts the value key into the HashSet.
- bool contains(key) Returns whether the value key exists in the
HashSet or not.
- void remove(key) Removes the value key in the HashSet. If key does not exist
in the HashSet, do nothing.

https://leetcode.com/problems/design-hashset/description/
"""


class MyHashSet:

    def __init__(self):
        self.storage = set()

    def add(self, key: int) -> None:
        self.storage.add(key)
        return True

    def remove(self, key: int) -> None:
        if key in self.storage:
            self.storage.remove(key)
            return True
        return False

    def contains(self, key: int) -> bool:
        if key in self.storage:
            return True
        return False
