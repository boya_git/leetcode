"""
You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order, and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Example:
Input: l1 = [2,4,3], l2 = [5,6,4]
Output: [7,0,8]
Explanation: 342 + 465 = 807.

https://leetcode.com/problems/add-two-numbers/
"""

from typing import Optional, Union

class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


def to_str(ll: Optional[ListNode], total: str = '') -> Union[ListNode, str]:
    total += str(ll.val)
    if ll.next == None:
        return total
    return to_str(ll.next, total)


def to_linked_list(lst=[0,], next=None):
    try:
        val = lst.pop()
    except IndexError:
        return next
    current = ListNode(val, next)
    return to_linked_list(lst, current)


class Solution:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        n1r = int(to_str(l1)[::-1])
        n2r = int(to_str(l2)[::-1])
        result = list(map(int, str(n1r+n2r)[::-1]))
        print(result)
        return to_linked_list(result)


if __name__ == '__main__':
    s = Solution()
    s.addTwoNumbers(to_linked_list([2,4,3]), to_linked_list([5,6,4])) # [7, 0, 8]
    s.addTwoNumbers(to_linked_list([0]), to_linked_list([0])) # [0]
    s.addTwoNumbers(to_linked_list([9,9,9,9,9,9,9]), to_linked_list([9,9,9,9])) # [8, 9, 9, 9, 0, 0, 0, 1]
    print('Good job!')
