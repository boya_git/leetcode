"""
https://leetcode.com/problems/longest-increasing-subsequence/description/

    Given an integer array nums, return the length of the longest strictly
increasing subsequence.
"""


class Solution:
    def lengthOfLIS(self, nums: list[int]) -> int:
        n = len(nums)
        cache = [1] * n
        for i in range(1, n):
            for j in range(i):
                if nums[i] > nums[j]:
                    cache[i] = max(cache[i], cache[j] + 1)
        return max(cache)
