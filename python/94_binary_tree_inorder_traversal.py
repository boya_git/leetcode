"""
Given the root of a binary tree, return the inorder traversal of its nodes' values.

https://leetcode.com/problems/binary-tree-inorder-traversal/
"""

from typing import Optional, List

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def inorderTraversal(self, root: Optional[TreeNode]) -> List[int]:
        self.answer = []
        if root is None: return self.answer
        def travesal(node: Optional[TreeNode]):
            if node.left: travesal(node.left)
            self.answer.append(node.val)
            if node.right: travesal(node.right)
        travesal(root)
        return self.answer