"""
You are given a 0-indexed integer array costs where costs[i] is the cost of
hiring the ith worker.

You are also given two integers k and candidates. We want to hire exactly k
workers according to the following rules:
- You will run k sessions and hire exactly one worker in each session.
- In each hiring session, choose the worker with the lowest cost from either
  the first candidates workers or the last candidates workers. Break the tie by
  the smallest index.
    - For example, if costs = [3,2,7,7,1,2] and candidates = 2, then in the
      first hiring session, we will choose the 4th worker because they have the
      lowest cost [3,2,7,7,1,2].
    - In the second hiring session, we will choose 1st worker because they have
      the same lowest cost as 4th worker but they have the smallest index
      [3,2,7,7,2]. Please note that the indexing may be changed in the process.

- If there are fewer than candidates workers remaining, choose the worker with
  the lowest cost among them. Break the tie by the smallest index.
- A worker can only be chosen once.

Return the total cost to hire exactly k workers.

https://leetcode.com/problems/total-cost-to-hire-k-workers/description/
"""


import heapq

class Solution:
    # Runtime 772 ms (Beats 89.66%)
    # Memory 27.1 MB (Beats 62.15%)
    def totalCost(self, costs: list[int], k: int, candidates: int) -> int:
        answer = 0
        left, right = candidates-1, len(costs)-candidates
        
        if left+1 >= right:
            costs.sort()
            return sum(costs[:k])

        left_heap = costs[:left+1]
        right_heap = costs[right:]
        heapq.heapify(left_heap)
        heapq.heapify(right_heap)

        for _ in range(k):
            if left_heap[0] <= right_heap[0]:
                answer += heapq.heappop(left_heap)
                if right - left > 1:
                    left += 1
                    heapq.heappush(left_heap, costs[left])
                if not left_heap:
                    left_heap.append(float("inf"))
            else:
                answer += heapq.heappop(right_heap)
                if right - left > 1:
                    right -=1
                    heapq.heappush(right_heap, costs[right])
                if not right_heap:
                    right_heap.append(float("inf"))

        return answer
