"""
You are given a network of n nodes, labeled from 1 to n. You are also given
times, a list of travel times as directed edges times[i] = (ui, vi, wi), where
ui is the source node, vi is the target node, and wi is the time it takes for a
signal to travel from source to target.

We will send a signal from a given node k. Return the time it takes for all the
n nodes to receive the signal. If it is impossible for all the n nodes to
receive the signal, return -1.

https://leetcode.com/problems/network-delay-time/
"""

import collections
import heapq

from typing import List


# (Used hint from discussion)
class Solution:
    def networkDelayTime(self, times: List[List[int]], n: int, k: int) -> int:
        graph = collections.defaultdict(list)
        queue = [(0, k)]
        t = {}

        for x, y, w in times:
            graph[x].append((y, w))

        while(queue):
            time, node = heapq.heappop(queue)
            if node not in t:
                t[node] = time
                for v, w in graph[node]:
                    heapq.heappush(queue, (time + w, v))

        return max(t.values()) if len(t) == n else -1
