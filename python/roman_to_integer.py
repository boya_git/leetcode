"""
Roman numerals are represented by seven different symbols: I, V, X, L, C, D and M.

Symbol       Value
I             1
V             5
X             10
L             50
C             100
D             500
M             1000
For example, 2 is written as II in Roman numeral, just two one's added together. 12 is written as XII, which is simply X + II. The number 27 is written as XXVII, which is XX + V + II.

Roman numerals are usually written largest to smallest from left to right. However, the numeral for four is not IIII. Instead, the number four is written as IV. Because the one is before the five we subtract it making four. The same principle applies to the number nine, which is written as IX. There are six instances where subtraction is used:

I can be placed before V (5) and X (10) to make 4 and 9. 
X can be placed before L (50) and C (100) to make 40 and 90. 
C can be placed before D (500) and M (1000) to make 400 and 900.
Given a roman numeral, convert it to an integer.

https://leetcode.com/problems/roman-to-integer/
"""

ROMAN_NUM_ADV = {
    'I':  1,
    'IV': 4,
    'V':  5,
    'IX': 9,
    'X':  10,
    'XL': 40,
    'L':  50,
    'XC': 90,
    'C':  100,
    'CD': 400,
    'D':  500,
    'CM': 900,
    'M':  1000,
}


class Solution:
    def romanToInt(self, s: str) -> int:
        total = 0
        i = len(s)-1
        while i >= 0:
            if s[i-1:i+1] in ROMAN_NUM_ADV:
                total += ROMAN_NUM_ADV[s[i-1:i+1]]
                i -= 1
            else:
                total += ROMAN_NUM_ADV[s[i]]
            i -= 1
        return(total)

    def romanToInt2(self, s: str) -> int:
        prev = s[-1]
        total = ROMAN_NUM_ADV[prev]
        for curr in reversed(s[:-1]):
            if ROMAN_NUM_ADV[curr] < ROMAN_NUM_ADV[prev]:
                total -= ROMAN_NUM_ADV[curr]
            else:
                total += ROMAN_NUM_ADV[curr]
            prev = curr
        return(total)


s = Solution()
assert(s.romanToInt("I")) == 1
assert(s.romanToInt("IX")) == 9
assert(s.romanToInt("III")) == 3 
assert(s.romanToInt("LVIII")) == 58
assert(s.romanToInt("MCMXCIV")) == 1994
assert(s.romanToInt("MIII")) == 1003
print('Good job!')
