"""
https://leetcode.com/problems/path-crossing/description/

    Given a string path, where path[i] = 'N', 'S', 'E' or 'W', each representing
moving one unit north, south, east, or west, respectively. You start at the
origin (0, 0) on a 2D plane and walk on the path specified by path.

    Return true if the path crosses itself at any point, that is, if at any time
you are on a location you have previously visited. Return false otherwise.
"""


class Solution:
    def isPathCrossing(self, path: str) -> bool:
        seen = {(0, 0)}
        current = [0,0]
        steps_map = {
            'N': [0, 1],
            'S': [0, -1],
            'E': [-1, 0],
            'W': [1, 0],
        }
        for step in path:
            x, y = steps_map.get(step)
            current[0] += x
            current[1] += y
            if tuple(current) in seen:
                return True
            seen.add(tuple(current))
        return False
