"""
Given the root of a binary tree, the level of its root is 1, the level of its
children is 2, and so on.

Return the smallest level x such that the sum of all the values of nodes at
level x is maximal.

https://leetcode.com/problems/maximum-level-sum-of-a-binary-tree/description/
"""
class Solution:
    def maxLevelSum(self, root) -> int:
        self.sums = {}

        def travesal(self, unit, lvl):
            if lvl not in self.sums:
                self.sums[lvl] = 0
            self.sums[lvl] += unit.val
            if unit.left:
                travesal(self, unit.left, lvl + 1)
            if unit.right:
                travesal(self, unit.right, lvl + 1)
            return

        travesal(self, root, 1)
        max_sum = max(self.sums.items(), key=lambda x: x[1])[1]
        return min(filter(lambda x: x[1] == max_sum, self.sums.items()), key=lambda x: x[0])[0]


class Solution2:
    def maxLevelSum(self, root) -> int:
        self.sums = []

        def travesal(self, unit, lvl):
            if len(self.sums) < lvl:
                self.sums.append(0)
            self.sums[lvl-1] += unit.val
            if unit.left:
                travesal(self, unit.left, lvl + 1)
            if unit.right:
                travesal(self, unit.right, lvl + 1)
            return

        travesal(self, root, 1)
        
        return max(enumerate(self.sums, 1), key=lambda x: x[1])[0]
