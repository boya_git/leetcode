"""
https://leetcode.com/problems/top-k-frequent-elements/

Given an integer array nums and an integer k, return the k most frequent
elements. You may return the answer in any order.
"""

from typing import List


class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
        frequent = dict()

        for x in nums:
            frequent[x] = frequent.get(x, 0) + 1

        answer = sorted(frequent.items(), key=lambda x: x[1], reverse=True)[:k]
        return [k for k, _ in answer]
