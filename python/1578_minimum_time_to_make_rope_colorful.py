"""
https://leetcode.com/problems/minimum-time-to-make-rope-colorful/description/

    Alice has n balloons arranged on a rope. You are given a 0-indexed string
colors where colors[i] is the color of the ith balloon.

    Alice wants the rope to be colorful. She does not want two consecutive
balloons to be of the same color, so she asks Bob for help. Bob can remove some
balloons from the rope to make it colorful. You are given a 0-indexed integer
array neededTime where neededTime[i] is the time (in seconds) that Bob needs to
remove the ith balloon from the rope.

    Return the minimum time Bob needs to make the rope colorful.
"""


class Solution:
    def minCost(self, colors: str, neededTime: list[int]) -> int:
        ans = 0
        prev = (colors[0], neededTime[0])
        for curr, time in zip(colors[1:], neededTime[1:]):
            if curr == prev[0]:
                if time <= prev[1]:
                    ans += time
                else:
                    ans += prev[1]
                    prev = (curr, time)
            else:
                prev = (curr, time)
        return ans
