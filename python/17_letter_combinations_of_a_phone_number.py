"""
https://leetcode.com/problems/letter-combinations-of-a-phone-number/description/

    Given a string containing digits from 2-9 inclusive, return all possible
letter combinations that the number could represent. Return the answer in any
order.
    A mapping of digits to letters (just like on the telephone buttons) is given
below. Note that 1 does not map to any letters.
"""

from itertools import product


class Solution:
    def letterCombinations(self, digits: str) -> list[str]:
        if not digits:
            return None
        digits_letters = {'1': None, '2': "abc", '3': "def",
                          '4': "ghi", '5': "jkl", '6': "mno",
                          '7': "pqrs", '8': "tuv", '9': "wxyz", }
        answer = []

        def create_combinations(idx: int, part: str) -> list:
            if idx >= len(digits):
                answer.append(part)
                return None
            for letter in digits_letters[digits[idx]]:
                create_combinations(idx+1, part+letter)
        create_combinations(0, "")
        return answer

    def letterCombinationsUsingItertoolsProduct(self, digits: str) -> list[str]:
        if not digits:
            return None
        digits_letters = {'1': None, '2': "abc", '3': "def",
                          '4': "ghi", '5': "jkl", '6': "mno",
                          '7': "pqrs", '8': "tuv", '9': "wxyz",}
        return (''.join(comb) for comb in product(*[digits_letters.get(d) for d in digits]))
