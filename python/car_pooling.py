"""
There is a car with capacity empty seats. The vehicle only drives east (i.e., it cannot turn around and drive west).

You are given the integer capacity and an array trips where trip[i] = [numPassengersi, fromi, toi] indicates that the ith trip has numPassengersi passengers and the locations to pick them up and drop them off are fromi and toi respectively. The locations are given as the number of kilometers due east from the car's initial location.

Return true if it is possible to pick up and drop off all passengers for all the given trips, or false otherwise.

https://leetcode.com/problems/car-pooling/
"""

from typing import List


class Solution:
    def carPooling(self, trips: List[List[int]], capacity: int) -> bool:
        roadmap = dict()
        for trip in trips:
            trip_capacity, start, end = trip
            for i in range(start, end):
                roadmap[i] = roadmap.get(i, 0) + trip_capacity
        max_capacity = max(roadmap.values())
        return max_capacity <= capacity

s = Solution()
print(s.carPooling(trips=[[2,1,5],[3,3,7]], capacity=4))
print(s.carPooling(trips=[[2,1,5],[3,3,7]], capacity=5))
print(s.carPooling(trips=[[2,1,5],[3,5,7]], capacity=3))
