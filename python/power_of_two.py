"""
Given an integer n, return true if it is a power of two. Otherwise, return false.
An integer n is a power of two, if there exists an integer x such that n == 2^x.
https://leetcode.com/problems/power-of-two/
"""

import math

class Solution:
    
    def isPowerOfTwo(self, n: int) -> bool:
        if n <= 0:
            return False
        power = math.log2(abs(n))
        return int(power) == power


s = Solution()
assert(s.isPowerOfTwo(n=1)) == True
assert(s.isPowerOfTwo(n=8)) == True
assert(s.isPowerOfTwo(n=16)) == True
assert(s.isPowerOfTwo(n=3)) == False
assert(s.isPowerOfTwo(n=536870912)) == True
assert(s.isPowerOfTwo(n=0)) == False
assert(s.isPowerOfTwo(n=-3)) == False
assert(s.isPowerOfTwo(n=-16)) == False
print('Good job!')
