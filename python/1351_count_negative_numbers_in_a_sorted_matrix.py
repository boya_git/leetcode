"""
Given a m x n matrix grid which is sorted in non-increasing order both row-wise
and column-wise, return the number of negative numbers in grid.

https://leetcode.com/problems/count-negative-numbers-in-a-sorted-matrix/description/
"""


class Solution:
    def countNegatives(self, grid: list[list[int]]) -> int:
        return sum([sum(map(lambda x: x < 0, line)) for line in grid])
