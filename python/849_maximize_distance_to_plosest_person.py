"""
You are given an array representing a row of seats where seats[i] = 1 represents a person sitting in the ith seat, and seats[i] = 0 represents that the ith seat is empty (0-indexed).

There is at least one empty seat, and at least one person sitting.

Alex wants to sit in the seat such that the distance between him and the closest person to him is maximized. 

Return that maximum distance to the closest person.

https://leetcode.com/problems/maximize-distance-to-closest-person/
"""

from typing import List


class Solution:
    def maxDistToClosest(self, seats: List[int]) -> int:
        start, end = 0, 0
        counter, answer = 0, 0
        for i in range(len(seats)):
            if not seats[i]:
                counter += 1
                end = i
            else:
                if start == 0 or end == len(seats)-1:
                    answer = max(answer, counter)
                else:
                    answer = max(answer, counter//2 + counter%2)
                counter = 0
                start = i+1
        if start == 0 or end == len(seats)-1:
            answer = max(answer, counter)
        else:
            answer = max(answer, counter//2 + counter%2)
        return answer
     