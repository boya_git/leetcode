"""
Given an array of points where points[i] = [xi, yi] represents a point on the X-Y plane and an integer k, return the k closest points to the origin (0, 0).

The distance between two points on the X-Y plane is the Euclidean distance (i.e., √(x1 - x2)2 + (y1 - y2)2).

You may return the answer in any order. The answer is guaranteed to be unique (except for the order that it is in).

https://leetcode.com/problems/k-closest-points-to-origin/
"""



from typing import List
from math import sqrt

class Solution:
    origin = (0, 0)
    def kClosest(self, points: List[List[int]], k: int) -> List[List[int]]:
        points.sort(key=lambda x: sqrt(pow(x[0]-self.origin[0],2)+pow(x[1]-self. origin[1],2)))
        return points[:k]

s = Solution()
inputs = [
    {'q': ([[1,3],[-2,2]], 1), 'ans': [[-2, 2]]},
    {'q': ([[3,3],[5,-1],[-2,4]], 2), 'ans': [[3, 3], [-2, 4]]},
]
for input in inputs:
    assert(s.kClosest(*input['q'])) == input['ans']

print('Good job!')
