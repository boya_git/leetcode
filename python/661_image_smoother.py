"""
https://leetcode.com/problems/image-smoother/description/

    An image smoother is a filter of the size 3 x 3 that can be applied to each
cell of an image by rounding down the average of the cell and the eight
surrounding cells (i.e., the average of the nine cells in the blue smoother). If
one or more of the surrounding cells of a cell is not present, we do not consider
it in the average (i.e., the average of the four cells in the red smoother).

    Given an m x n integer matrix img representing the grayscale of an image,
return the image after applying the smoother on each cell of it.
"""


class Solution:
    def imageSmoother(self, img: list[list[int]]) -> list[list[int]]:
        rows = len(img)
        cols = len(img[0])

        def calc_aver(i: int, j: int) -> int:
            l = [img[si][sj]
                 for sj in range(max(0, j-1), min(cols, j+2))
                 for si in range(max(0, i-1), min(rows, i+2))]
            return sum(l) // len(l)

        return [[calc_aver(i, j) for j in range(cols)]
                 for i in range(rows)]
