"""
https://leetcode.com/problems/range-sum-of-bst/description/

    Given the root node of a binary search tree and two integers low and high,
return the sum of values of all nodes with a value in the inclusive range
[low, high].
"""


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ans = 0

    def rangeSumBST(self, root: TreeNode | None, low: int, high: int) -> int:

        def traversal(node: TreeNode):
            if low <= node.val <= high:
                self.ans += node.val
            if node.left: traversal(node.left)
            if node.right: traversal(node.right)

        traversal(root)
        return self.ans
