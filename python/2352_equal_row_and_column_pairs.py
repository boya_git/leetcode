"""
Given a 0-indexed n x n integer matrix grid, return the number of pairs
(ri, cj) such that row ri and column cj are equal.

A row and column pair is considered equal if they contain the same elements in
the same order (i.e., an equal array).

https://leetcode.com/problems/equal-row-and-column-pairs/description/
"""


class Solution:
    # Runtime 1098 ms (Beats 13.18%) | Memory 20.9 MB (Beats 93.56%)
    def equalPairs(self, grid: list[list[int]]) -> int:
        counter = 0
        for row in grid:
            row = tuple(row)
            for col in zip(*grid):
                if row == col:
                    counter += 1
        return counter
