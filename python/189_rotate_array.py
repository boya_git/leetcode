"""
https://leetcode.com/problems/rotate-array/

Given an array, rotate the array to the right by k steps, where k is non-negative.

Follow up:

Try to come up with as many solutions as you can. There are at least three
different ways to solve this problem. Could you do it in-place with O(1) extra
space?
"""
from typing import List


class Solution:
    def rotate(self, nums: List[int], k: int) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """

        # if K more than lenght of input data, we must remove the extra
        while k > len(nums):
            k -= len(nums)

        last = len(nums)-1
        nums_c = nums.copy()

        new_indexes = [i+k if i+k <= last else (i+k)-last-1
                       for i in range(len(nums))]

        for i in range(len(nums)):
            nums[new_indexes[i]] = nums_c[i]
