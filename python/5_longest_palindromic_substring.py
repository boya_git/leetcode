"""
Given a string s, return the longest palindromic substring in s.

https://leetcode.com/problems/longest-palindromic-substring/
"""


class Solution:
    def longestPalindrome(self, s: str) -> str:
        if not s: return ''
        if s == s[::-1]: return s

        palindrome = s[0]
        for i in range(len(s)):
            tmp = self.extend_palindrome(s,i,i)
            if len(palindrome) < len(tmp):
                palindrome = tmp

            tmp = self.extend_palindrome(s,i,i+1)
            if len(palindrome) < len(tmp):
                palindrome = tmp

        return palindrome

    def extend_palindrome(self, s: str, l: int, r: int):
        while l >= 0 and r <= len(s)-1 and s[l] == s[r]:
            l -= 1
            r += 1
        return s[l+1:r]
