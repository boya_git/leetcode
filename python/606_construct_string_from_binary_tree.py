"""
https://leetcode.com/problems/construct-string-from-binary-tree/description/

Given the root of a binary tree, construct a string consisting of parenthesis
and integers from a binary tree with the preorder traversal way, and return it.

Omit all the empty parenthesis pairs that do not affect the one-to-one mapping
relationship between the string and the original binary tree.
"""

from typing import Optional


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def tree2str(self, root: Optional[TreeNode]) -> str:
        if root is None:
            return ""
        if root.left is None and root.right is None:
            return str(root.val)
        left_part = self.tree2str(root.left)
        right_part = self.tree2str(root.right)
        right_part = f"({right_part})" if right_part else right_part
        return f"{root.val}({left_part})" + right_part
    