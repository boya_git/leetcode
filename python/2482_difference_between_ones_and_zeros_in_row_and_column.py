"""
https://leetcode.com/problems/difference-between-ones-and-zeros-in-row-and-column/description/

You are given a 0-indexed m x n binary matrix grid.

A 0-indexed m x n difference matrix diff is created with the following procedure:
 - Let the number of ones in the ith row be onesRowi.
 - Let the number of ones in the jth column be onesColj.
 - Let the number of zeros in the ith row be zerosRowi.
 - Let the number of zeros in the jth column be zerosColj.
 - diff[i][j] = onesRowi + onesColj - zerosRowi - zerosColj

Return the difference matrix diff.
"""


class Solution:

    def onesMinusZeros(self, grid: list[list[int]]) -> list[list[int]]:
        len_rows = len(grid[0])
        len_cols = len(grid)
        rows_ones = [row.count(1) for row in grid]
        cols_ones = [col.count(1) for col in zip(*grid)]
        return [
            [
                2*(rows_ones[i] + cols_ones[j]) - len_rows - len_cols
                for j in range(len(grid[0]))
            ]
            for i in range(len(grid))
        ]
