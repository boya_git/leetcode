"""
Given an m x n binary matrix filled with 0's and 1's, find the largest square containing only 1's and return its area.

https://leetcode.com/problems/maximal-square/
"""

from typing import List


class Solution:
    def maximalSquare(self, matrix: List[List[str]]) -> int:
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                if j-1 < 0 or i-1 < 0 or int(matrix[i][j]) == 0:
                    matrix[i][j] = int(matrix[i][j])
                else:
                    tmp = int(matrix[i][j])
                    matrix[i][j] = min(
                        int(matrix[i][j-1]),
                        int(matrix[i-1][j-1]),
                        int(matrix[i-1][j]),
                    )
                    if matrix[i][j]:
                        matrix[i][j] += 1
                    else:
                       matrix[i][j] = tmp
        return max(max(line) for line in matrix)
