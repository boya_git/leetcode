class Solution:
    def spiralOrder(self, matrix: list[list[int]]) -> list[int]:
        size = len(matrix) * len(matrix[0])
        i_max = len(matrix)-1
        j_max = len(matrix[0])-1
        i_min = 1
        j_min = 0

        turns = {
            'right': [0, 1],
            'bottom': [1, 0],
            'left': [0, -1],
            'up': [-1, 0]
        }

        i, j = 0,0
        step = turns.get('right')

        for _ in range(size):
            yield matrix[i][j]
            if step == turns.get('right') and j == j_max:
                j_max -= 1
                step = turns.get('bottom')
            if step == turns.get('bottom') and i == i_max:
                i_max -= 1
                step = turns.get('left')
            if step == turns.get('left') and j == j_min:
                j_min += 1
                step = turns.get('up')
            if step == turns.get('up') and i == i_min:
                i_min += 1
                step = turns.get('right')
            i += step[0]
            j += step[1]
