"""
You have a long flowerbed in which some of the plots are planted, and some are not. However, flowers cannot be planted in adjacent plots.

Given an integer array flowerbed containing 0's and 1's, where 0 means empty and 1 means not empty, and an integer n, return if n new flowers can be planted in the flowerbed without violating the no-adjacent-flowers rule.

https://leetcode.com/problems/can-place-flowers/
"""

from typing import List


class Solution:
    def canPlaceFlowers(self, flowerbed: List[int], n: int) -> bool:
        l = len(flowerbed)
        counter = 0
        i = 1
        
        if l == 1 and not flowerbed[0]:
            return n <= 1

        if n > len(flowerbed)//2+len(flowerbed)%2:
            return False
        
        if not flowerbed[0] and not flowerbed[1]:
            counter += 1
            i += 1

        if not flowerbed[l-1] and not flowerbed[l-2]:
            counter += 1
            l -= 1

        while i <= l-2:
            if flowerbed[i]:
                i += 1
                continue
            
            if not flowerbed[i-1]+flowerbed[i+1]:
                counter += 1
                i += 1
            i += 1
        return n <= counter
