from itertools import chain
from typing import List

class Solution:
    # 40ms Beats 54.52%of users with Python3
    # 16.33MB Beats 28.92%of users with Python3
    def arrayStringsAreEqual(self, word1: List[str], word2: List[str]) -> bool:
        return ''.join(word1) == ''.join(word2)


class Solution2:
    # 32ms Beats 92.88% of users with Python3
    # 16.42MB Beats 28.92% of users with Python3
    def arrayStringsAreEqual(self, word1: List[str], word2: List[str]) -> bool:
        if sum(len(w) for w in word1) != sum(len(w) for w in word2):
            return False

        for l1, l2 in zip(chain(*word1), chain(*word2)):
            if l1 != l2: return False
            return True