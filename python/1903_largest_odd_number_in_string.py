"""
https://leetcode.com/problems/largest-odd-number-in-string/description/

    You are given a string num, representing a large integer. Return the
largest-valued odd integer (as a string) that is a non-empty substring of
num, or an empty string "" if no odd integer exists.

    A substring is a contiguous sequence of characters within a string.
"""


class Solution:
    def largestOddNumber(self, num: str) -> str:
        for ri in range(len(num)-1, -1, -1):
            if int(num[ri]) % 2:
                return num[:ri+1]
            return ""

# I can do this, cuz its much better!
# return num.rstrip('02468')
# rstrip will remove all digits from right of string if it is equal to 02468