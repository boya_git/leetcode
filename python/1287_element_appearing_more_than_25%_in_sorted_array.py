"""
https://leetcode.com/problems/element-appearing-more-than-25-in-sorted-array/description/

    Given an integer array sorted in non-decreasing order, there is exactly
one integer in the array that occurs more than 25% of the time, return that
integer.
"""


class Solution:
    def findSpecialInteger(self, arr: list[int]) -> int:
        freq25 = len(arr) * 0.25
        elem = arr[0]
        freq = 0
        for item in arr:
            if item != elem:
                elem = item
                freq = 0
            freq += 1
            if freq > freq25:
                return item
