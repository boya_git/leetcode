"""
https://leetcode.com/problems/maximum-depth-of-binary-tree/

Given the root of a binary tree, return its maximum depth.

A binary tree's maximum depth is the number of nodes along the longest path
from the root node down to the farthest leaf node.
"""
from typing import Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def maxDepth(self, root: Optional[TreeNode]) -> int:
        self.ans = 0

        if not root:
            return self.ans

        def go_deep(current, counter):
            counter += 1
            if not current.left and not current.right:
                self.ans = max(self.ans, counter)
                return

            if current.left:
                go_deep(current.left, counter)
            if current.right:
                go_deep(current.right, counter)
        go_deep(root, self.ans)
        return self.ans

# Runtime: 55 ms, faster than 50.62% of Python3 online submissions.
# Memory Usage: 16.3 MB, less than 31.05% of Python3 online submissions.
