class Solution:
    # The solution is not mine, but I understood and learned it.
    # Solution: https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/solutions/3667440/beats-100-c-java-python-beginner-friendly/
    def maxProfit(self, prices: list[int], fee: int) -> int:
        buy = float("-inf") # maximum profit after buying stocks
        sell = 0 # maximum profit after selling stocks

        for price in prices:
            buy = max(buy, sell - price) # Will today's buy balance exceed the previous high?
            sell = max(sell, buy + price - fee) # Will this sell deal be better than the previous high?

        return sell
