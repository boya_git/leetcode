"""
https://leetcode.com/problems/maximum-product-of-two-elements-in-an-array/description/

    Given the array of integers nums, you will choose two different indices
i and j of that array. Return the maximum value of (nums[i]-1)*(nums[j]-1).
"""


class Solution:

    # Runtime 57 ms Beats 53.82%
    # Memory 16.4 MB Beats 10.60%
    def maxProduct(self, nums: list[int]) -> int:
        max1, max2 = 0, 0
        for item in nums:
            if item > max1:
                max2 = max1
                max1 = item
            elif item > max2:
                max2 = item
        return (max1-1)*(max2-1)

    # Runtime 55 ms Beats 64.56%
    # Memory 16.2 MB Beats 75.6%
    def maxProduct2(self, nums: list[int]) -> int:
        a, b = sorted(nums)[-2:]
        return (a-1)*(b-1)
