"""
https://leetcode.com/problems/single-number/

Given a non-empty array of integers nums, every element appears twice except
for one. Find that single one.

You must implement a solution with a linear runtime complexity and use only
constant extra space.
"""
from typing import List


class Solution:
    def singleNumber(self, nums: List[int]) -> int:
        items = set()
        for item in nums:
            if item not in items:
                items.add(item)
            else:
                items.remove(item)
        return items.pop()

    # Not my!
    def singleNumber2(self, nums: List[int]) -> int:
        total = 0
        for item in nums:
            total ^= item
        return total

# Runtime: 132 ms, faster than 86.40% of Python3 online submissions.
# Memory Usage: 17.2 MB, less than 6.05% of Python3 online submissions.
