"""
https://leetcode.com/problems/design-a-food-rating-system/description/


    Design a food rating system that can do the following:

 - Modify the rating of a food item listed in the system.
 - Return the highest-rated food item for a type of cuisine in the system.
   Implement the FoodRatings class:

 - FoodRatings(String[] foods, String[] cuisines, int[] ratings) Initializes the
   system. The food items are described by foods, cuisines and ratings, all of
   which have a length of n.
   - foods[i] is the name of the ith food,
   - cuisines[i] is the type of cuisine of the ith food, and
   - ratings[i] is the initial rating of the ith food.
 - void changeRating(String food, int newRating) Changes the rating of the food
   item with the name food.
 - String highestRated(String cuisine) Returns the name of the food item that has
   the highest rating for the given type of cuisine. If there is a tie, return
   the item with the lexicographically smaller name.

    Note that a string x is lexicographically smaller than string y if x comes
before y in dictionary order, that is, either x is a prefix of y, or if i is the
first position such that x[i] != y[i], then x[i] comes before y[i] in alphabetic
order.
"""


from sortedcontainers import SortedList

class FoodRatings:

    def __init__(self, foods: list[str], cuisines: list[str], ratings: list[int]):
        self.food_data = {}
        self.sorted_cuisines = defaultdict(SortedList)

        for cuisine, food, rating in zip(cuisines, foods, ratings):
            self.food_data[food] = (cuisine, rating)
            self.sorted_cuisines[cuisine].add((-rating, food))

    def changeRating(self, food: str, newRating: int) -> None:
        cuisine, rating = self.food_data.get(food)
        self.food_data[food] = (cuisine, newRating)
        self.sorted_cuisines[cuisine].discard((-rating, food))
        self.sorted_cuisines[cuisine].add((-newRating, food))

    def highestRated(self, cuisine: str) -> str:
        return self.sorted_cuisines[cuisine][0][1]
