"""
In a town, there are n people labeled from 1 to n. There is a rumor that one of these people is secretly the town judge.

If the town judge exists, then:

The town judge trusts nobody.
Everybody (except for the town judge) trusts the town judge.
There is exactly one person that satisfies properties 1 and 2.
You are given an array trust where trust[i] = [ai, bi] representing that the person labeled ai trusts the person labeled bi.

Return the label of the town judge if the town judge exists and can be identified, or return -1 otherwise.

https://leetcode.com/problems/find-the-town-judge/
"""

from typing import List

class Solution:
    def findJudge(self, n: int, trust: List[List[int]]) -> int:
        if not trust and n == 1:
            return n

        peoples = dict()
        for pair in trust:
            peoples[pair[0]] = peoples.get(pair[0], 0) + 1
            peoples[pair[1]] = peoples.get(pair[1], 0)
        
        judge = 0
        for key, value in peoples.items():
            if value == 0:
                if not judge:
                    judge = key
                else:
                    return -1

        if judge:
            del peoples[judge]
            for pair in trust:
                if pair[1] == judge:
                    peoples[pair[0]] = 0

        return judge if judge and not sum(peoples.values()) else -1
