"""
Given an array nums of distinct integers, return all the possible permutations.
You can return the answer in any order.

https://leetcode.com/problems/permutations/description/
"""


from itertools import permutations

class Solution:
    def permute(self, nums: list[int]) -> list[list[int]]:
        return permutations(nums) #:)