"""
https://leetcode.com/problems/increasing-order-search-tree/

Given the root of a binary search tree, rearrange the tree in in-order so that
the leftmost node in the tree is now the root of the tree, and every node has
no left child and only one right child.
"""


# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def increasingBST(self, root: TreeNode) -> TreeNode:
        self.head = None
        self.current = None

        def traversal(root: TreeNode):
            if root.left:
                traversal(root.left)
            if self.head is None:
                self.head = root
                self.current = self.head
            else:
                self.current.right = root
                self.current.left = None
                self.current = self.current.right
            if root.right:
                traversal(root.right)

        traversal(root)
        self.current.left = None
        self.current.right = None
        return self.head


# Runtime: 39 ms, faster than 66.24% of Python3 online submissions.
# Memory Usage: 13.9 MB, less than 90.59% of Python3 online submissions.
