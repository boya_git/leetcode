"""
You are given an array coordinates, coordinates[i] = [x, y], where [x, y]
represents the coordinate of a point. Check if these points make a straight
line in the XY plane.

https://leetcode.com/problems/check-if-it-is-a-straight-line/description/
"""

class Solution:
    def checkStraightLine(self, coordinates: list[list[int]]) -> bool:
        x1, y1, x2, y2 = *coordinates[0], *coordinates[-1]
        dY = (y1 - y2)
        dX = (x1 - x2)
        if not dX:
            formula = lambda c: c[0] == x1
        elif not dY:
            formula = lambda c: c[1] == x2
        else:
            k = dY / dX
            b = y2 - k * x2
            formula = lambda c: c[1] == k * c[0] + b
        return all(map(formula, coordinates))
